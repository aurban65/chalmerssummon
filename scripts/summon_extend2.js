$(document).ready(function() {
	// OsynliggÃ¶r hjÃ¤lplÃ¤nk i authorizationbanner men ligger kvar i hamburgermenyn.
	$("style").append("#content .search .header div .vpnBanner .ng-scope .list-inline .ng-scope .help { display: none }");
	
	// Ã„ndra fÃ¤rg pÃ¥ Advanced.
	$("style").append("button.btn.btn-link.no-right-gutter.toggleAdvanced.hidden-xs.hidden-sm.ng-binding.ng-scope { color: #1e1e1e !important }");
	
	// Ã„ndra fÃ¤rg pÃ¥ Advanced pil.
	$("style").append("button.btn.btn-link.no-right-gutter.toggleAdvanced.hidden-xs.hidden-sm.ng-binding.ng-scope::after { border-top-color: #1e1e1e !important }");
	
	// Ã„ndra fÃ¤rg pÃ¥ lÃ¤nkar i "authorization banner". Utkommenterad eftersom det visade sig finnas instÃ¤llningar i Summonadmin.
	//$("style").append("#content .search .header div .vpnBanner .ng-scope .list-inline .ng-scope .customColorsSiteLink { color: #1e1e1e !important }");
});


angular.module("summonApp")
.run(["configService", "localizationService", function (config, lang) {
    
    var journalsTitleSwe = "Tidskrifter A-Ã–";
    var journalsUrlSwe = "http://cx2rl7ub5c.openurl.xml.serialssolutions.com/";
    var journalsTitleEng = "Journals A-Z";
    var journalsUrlEng = "http://cx2rl7ub5c.openurl.xml.serialssolutions.com/";

    var dbTitleSwe = "Databaser";
    var dbUrlSwe = "http://www.lib.chalmers.se/soek/databaser/";
    var dbTitleEng = "Databases";
    var dbUrlEng = "http://www.lib.chalmers.se/en/search/databases/";

    var illTitleSwe = "BestÃ¤ll material";
    var illUrlSwe = "http://www.lib.chalmers.se/soek/fjaerrlaan-och-inkoepsfoerslag/bestaell-material/";
    var illTitleEng = "Request material";
    var illUrlEng = "http://www.lib.chalmers.se/en/search/purchase-suggestions-and-interlibrary-loans/";

    var isSwe = (lang.localization.current.name == "Swedish");
    config.data.links.custom1 = {
        href: (isSwe ? journalsUrlSwe : journalsUrlEng),
        label: (isSwe ? journalsTitleSwe : journalsTitleEng)
    }
    config.data.links.custom2 = {
        href: (isSwe ? dbUrlSwe : dbUrlEng),
        label: (isSwe ? dbTitleSwe : dbTitleEng)
    }
    config.data.links.custom3 = {
        href: (isSwe ? illUrlSwe : illUrlEng),
        label: (isSwe ? illTitleSwe : illTitleEng) 
    }
}]);

// Inserts message at top of Summon2 search results to inform users of platform issues.


 /*(function(){
  var tId = setInterval(function() {
    if (document.readyState == "complete") onComplete()
  }, 11);

  function onComplete(){
    clearInterval(tId);
        var theDiv = document.getElementById('results');
        var newNode = document.createElement('div');
        newNode.innerHTML = '<div style="margin: 1em 5% 0 5%;">' +
                        '<p style="font-size: 16px; color: DarkOrange; text-align: left;">' +
                        'Welcome to our new initiative Kuggen at Lindholmen, where you can pick up your holds, return your books and study.' +
                        '</p></div>';
        theDiv.insertBefore( newNode, theDiv.firstChild );
  };

